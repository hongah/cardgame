package edu.ntnu.idatt2003;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.geometry.Insets;

/**
 * Class for the GUI of the CardGame Application.
 */
public class CardGameGUI extends Application {
  private DeckOfCards deck;
  private HandOfCards hand;
  private TextArea handDisplay;
  private Label sumLabel;
  private Label heartsLabel;
  private Label flushLabel;
  private Label queenSpadesLabel;


  /**
   * Initializes a deck, and shuffles it.
   */
  public void initDeck() {
    deck = new DeckOfCards();
    deck.shuffle();
  }


  /**
   * Initializes the user interface for the card game.
   */
  public void start(Stage primaryStage) {
    initDeck();

    // Create GUI components
    handDisplay = new TextArea();
    Button dealHandButton = new Button("Deal new hand");
    Button checkHandButton = new Button("Check this hand");
    sumLabel = new Label("Sum of the faces: ");
    heartsLabel = new Label("Cards of hearts: ");
    flushLabel = new Label("Flush: No");
    queenSpadesLabel = new Label("Queen of spades: No");

    // Set action on button
    dealHandButton.setOnAction(event -> {
      try {
        dealHand();
      } catch (IllegalArgumentException e) {
        handDisplay.setText(e.getMessage());
      }});
    checkHandButton.setOnAction(event -> checkHand());

    // Set up layout
    VBox root = new VBox(10, handDisplay, dealHandButton, checkHandButton, sumLabel, heartsLabel, flushLabel, queenSpadesLabel);
    root.setPadding(new Insets(10));
    Scene scene = new Scene(root, 500, 500);
    handDisplay.setPrefSize(50, 50);
    scene.getRoot().setStyle("-fx-font-size: 20px;");

    // Display the main scene
    primaryStage.setTitle("Card Game");
    primaryStage.setScene(scene);
    primaryStage.show();
  }


  /**
   * Deals new hand, and displays the hand.
   *
   * @throws IllegalArgumentException if not enough cards to deal new hand.
   */
  private void dealHand() throws IllegalArgumentException {
    hand = new HandOfCards(deck.dealHand(5));
    handDisplay.setText(hand.toString());
  }


  /**
   * Checks the current hand for sum, hearts, flush and queen of spades.
   */
  private void checkHand() {
    sumLabel.setText("Sum of the faces: " + hand.sumOfCardValues());

    String hearts = hand.heartsOnHand();
    heartsLabel.setText("Cards of hearts: " + (hearts.isEmpty() ? "No Hearts" : hearts));

    flushLabel.setText("Flush: " + (hand.isFiveFlush() ? "Yes" : "No"));

    queenSpadesLabel.setText("Queen of spades: " + (hand.hasQueenOfSpades() ? "Yes" : "No"));
  }

}
