package edu.ntnu.idatt2003;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Represents a deck of playing cards.
 */
public class DeckOfCards {
  private final List<PlayingCard> cards = new ArrayList<>();
  private final Random random = new Random();


  /**
   * Constructs a new DeckOfCards and initializes it with a full deck of shuffled cards.
   */
  public DeckOfCards() {
    char[] suits = {'S','H','D','C'};
    for (char suit : suits) {
      for (int face = 1; face <= 13; face++) {
        cards.add(new PlayingCard(suit, face));
      }
    }
  }

  /**
   * Shuffles the deck of cards.
   */
  public void shuffle() {
    Collections.shuffle(cards);
  }

  /**
   * Deals a hand with the specified number (n) of cards. Will throw IllegalArgumentException if there are not enough
   * cards in the deck to deal the requested number.
   *
   * @param n The number of cards to be dealt.
   * @return A list of PlayingCard objects representing the dealt hand.
   * @throws IllegalArgumentException if there are not enough cards in the deck to deal the requested number.
   */
  public List<PlayingCard> dealHand(int n) throws IllegalArgumentException {
    if (n > cards.size() || n < 1) {
      throw new IllegalArgumentException("Not enough cards in the deck to deal the hand.");
    }

    List<PlayingCard> hand = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      int index = random.nextInt(cards.size());
      hand.add(cards.remove(index));
    }
    return hand;
  }


  /**
   * Returns the number of cards left in the deck.
   *
   * @return The number of cards left in the deck.
   */
  public int cardsLeft() {
    return cards.size();
  }


}
