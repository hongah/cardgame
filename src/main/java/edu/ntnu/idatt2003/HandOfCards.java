package edu.ntnu.idatt2003;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a hand of cards, and methods for checking the hand.
 */
public class HandOfCards {
  private final List<PlayingCard> hand;


  /**
   * Constructs a HandOfCards object with the specified list of playing cards.
   *
   * @param hand a list of playing cards representing the hand.
   */
  public HandOfCards(List<PlayingCard> hand) {
    this.hand = hand;
  }


  /**
   * Calculates the sum from the faces of the cards on the hand.
   *
   * @return sum of faces on the hand
   */
  public int sumOfCardValues() {
    return hand.stream()
               .mapToInt(PlayingCard::getFace)
               .sum();
  }


  /**
   * Finds all hearts on the hand.
   *
   * @return the hearts if there are, "No Hearts" if there are none.
   */
  public String heartsOnHand() {
    String hearts = hand.stream()
                        .filter(card -> card.getSuit() == 'H')
                        .map(PlayingCard::getAsString)
                        .collect(Collectors.joining(" "));
    return hearts.isEmpty() ? "No Hearts" : hearts;
  }


  /**
   * Checks if the hand has a queen of spades.
   *
   * @return true if hand has queen of spades, false otherwise.
   */
  public boolean hasQueenOfSpades() {
    return hand.stream()
               .anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
  }

  /**
   * Checks if the hand is a five flush.
   *
   * @return true if hand is a five flush, false otherwise.
   */
  public boolean isFiveFlush() {
    return hand.stream()
               .collect(Collectors.groupingBy(PlayingCard::getSuit, Collectors.counting()))
               .values()
               .stream()
               .anyMatch(count -> count >= 5);
  }


  /**
   * The hand of cards, to a string.
   *
   * @return the hand in string format
   */
  @Override
  public String toString() {
    return hand.stream()
               .map(PlayingCard::getAsString)
               .collect(Collectors.joining(" "));
  }
}
