package edu.ntnu.idatt2003;

/**
 * Main class.
 */
public class Main {
  /**
   * Main method: Launches the application
   */
  public static void main(String[] args) {
    CardGameGUI.launch(CardGameGUI.class, args);
  }
}