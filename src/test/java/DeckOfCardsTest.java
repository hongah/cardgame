import edu.ntnu.idatt2003.DeckOfCards;
import edu.ntnu.idatt2003.PlayingCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

public class DeckOfCardsTest {
  private DeckOfCards deck;

  @BeforeEach
  void setUp() {
    deck = new DeckOfCards();
  }



  @Test
  @DisplayName("New deck should contain 52 cards")
  void testAmountInNewDeck() {
    assertEquals(52, deck.cardsLeft());
  }

  @Test
  @DisplayName("Shuffling the deck does not change the size")
  void testShuffle() {
    assertDoesNotThrow(() -> deck.shuffle());
    assertEquals(52, deck.cardsLeft());
  }

  @Test
  @DisplayName("Correct number of cards in hand after dealing a hand")
  void testDealHandSize() {
    int handSize = 5;
    List<PlayingCard> hand = deck.dealHand(handSize);
    assertEquals(handSize, hand.size());
  }

  @Test
  @DisplayName("Deck size is correctly reduced after dealing a hand")
  void testCardsLeftAfterDeal() {
    int handSize = 5;
    deck.dealHand(handSize);
    assertEquals(52 - handSize, deck.cardsLeft());
  }

  @Test
  @DisplayName("Dealing a hand with invalid hand size. Should throw IllegalArgumentException.")
  void testDealHandWithIllegalArgument() {
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(53));
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(0));
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(-1));
  }

  @Test
  @DisplayName("Dealing a hand with no more cards left in the deck. Should throw IllegalArgumentException.")
  void testEnoughCards() {
    deck.dealHand(52);
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(1));
  }
}
