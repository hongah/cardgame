import edu.ntnu.idatt2003.HandOfCards;
import edu.ntnu.idatt2003.PlayingCard;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {
  private HandOfCards hand;

  @BeforeEach
  void setUp() {
    List<PlayingCard> cards = Arrays.asList(
        new PlayingCard('S', 7),
        new PlayingCard('H', 10),
        new PlayingCard('D', 3)
    );
    hand = new HandOfCards(cards);
  }

  @Test
  @DisplayName("Correctly calculates the sum of a hand.")
  void sumOfCardValues() {
    assertEquals(7+10+3, hand.sumOfCardValues());
  }

  @Test
  @DisplayName("Correctly displays the hearts on hand.")
  void testHeartsOnHand() {
    assertEquals("H10", hand.heartsOnHand());
  }

  @Test
  @DisplayName("Correctly displays that there are no hearts on hand, whn correct.")
  void testNoHeartsOnHand() {
    List<PlayingCard> cards = Arrays.asList(
        new PlayingCard('D', 4),
        new PlayingCard('S', 9)
    );
    HandOfCards hand = new HandOfCards(cards);
    assertEquals("No Hearts", hand.heartsOnHand());
  }

  @Test
  @DisplayName("Returns true if hand has Queen of spades.")
  void testHasQueenOfSpadesTrue() {
    List<PlayingCard> cards = Arrays.asList(
        new PlayingCard('D', 9),
        new PlayingCard('H', 8),
        new PlayingCard('S', 12)
    );
    HandOfCards hand = new HandOfCards(cards);
    assertTrue(hand.hasQueenOfSpades());
  }

  @Test
  @DisplayName("Returns false if hand does not have Queen of spades.")
  void testHasQueenOfSpadesFalse() {
    List<PlayingCard> cards = Arrays.asList(
        new PlayingCard('D', 9),
        new PlayingCard('H', 8),
        new PlayingCard('C', 10)
    );
    HandOfCards hand = new HandOfCards(cards);
    assertFalse(hand.hasQueenOfSpades());
  }

  @Test
  @DisplayName("Returns true if hand is a five flush.")
  void testFiveFlushTrue() {
    List<PlayingCard> cards = Arrays.asList(
        new PlayingCard('D', 1),
        new PlayingCard('D', 2),
        new PlayingCard('D', 3),
        new PlayingCard('D', 4),
        new PlayingCard('D', 5)
    );
    HandOfCards hand = new HandOfCards(cards);
    assertTrue(hand.isFiveFlush());
  }

  @Test
  @DisplayName("Returns false if hand is not a five flush.")
  void testFiveFlushFalse() {
    List<PlayingCard> cards = Arrays.asList(
        new PlayingCard('D', 4),
        new PlayingCard('S', 9),
        new PlayingCard('C', 12),
        new PlayingCard('D', 7),
        new PlayingCard('D', 10)
    );
    HandOfCards hand = new HandOfCards(cards);
    assertFalse(hand.isFiveFlush());
  }

  @Test
  @DisplayName("Correctly displays the hand.")
  void testHandToString() {
    assertEquals("S7 H10 D3", hand.toString());
  }
}
