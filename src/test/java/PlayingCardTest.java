import edu.ntnu.idatt2003.PlayingCard;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {

  @Test
  @DisplayName("Should not throw IllegalArgumentException when valid parameters for constructor.")
  void testValidParametersInConstructor() {
    PlayingCard card = new PlayingCard('H', 10);
    assertEquals('H', card.getSuit());
    assertEquals(10, card.getFace());
  }

  @Test
  @DisplayName("Should throw IllegalArgumentException for invalid suit parameter for constructor.")
  void testInvalidSuit() {
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('X', 5));
  }

  @Test
  @DisplayName("Should throw IllegalArgumentException for invalid face parameter for constructor.")
  void testInvalidFace() {
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard('S', 15));
  }

  @Test
  @DisplayName("Correctly display card in format <SUIT><FACE>.")
  void testCardToString() {
    PlayingCard card = new PlayingCard('D', 3);
    assertEquals("D3", card.getAsString());
  }

  @Test
  @DisplayName("Two of the same cards are equals.")
  void testSameCardTrue() {
    PlayingCard card1 = new PlayingCard('C', 8);
    PlayingCard card2 = new PlayingCard('C', 8);
    assertEquals(card1, card2);
  }

  @Test
  @DisplayName("Two different cards are not equals.")
  void testSameCardFalse() {
    PlayingCard card1 = new PlayingCard('H', 11);
    PlayingCard card2 = new PlayingCard('D', 11);
    assertNotEquals(card1, card2);
  }
}
